// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyBqy_xJfl8CVC4xuGGmAFgKXF9zqEZkz-c',
    authDomain: 'pdtemplateanos5-gitlab.firebaseapp.com',
    databaseURL: 'https://pdtemplateanos5-gitlab.firebaseio.com',
    projectId: 'pdtemplateanos5-gitlab',
    storageBucket: 'pdtemplateanos5-gitlab.appspot.com',
    messagingSenderId: '1019120496955'
  }
};
