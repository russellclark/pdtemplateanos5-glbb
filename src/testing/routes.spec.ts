import { TestBed } from '@angular/core/testing';
import { RouterTestingModule} from '@angular/router/testing';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import {HomeComponent} from '../app/home/home.component';
import {AdminComponent} from '../app/home/admin/admin.component';
import { routes } from '../app/app.routes.module';

describe('Routing', () => {
  let router: Router;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent, AdminComponent ],
      providers: [ Location ],
      imports: [ RouterTestingModule.withRoutes(routes)]
    });
    router = TestBed.get(Router);
    location = TestBed.get(Location);

    router.initialNavigation();
  });

  it('navigate to "" redirects you to ""', () => {
    router.navigate(['']).then( () => {
      expect(location.path()).toBe('/about');
    });
  });

  it('navigate to "home" redirects you to "home"', () => {
    router.navigate(['home']).then( () => {
      expect(location.path()).toBe('/home');
    });
  });

  it('navigate to "about" redirects you to "about"', () => {
    router.navigate(['about']).then( () => {
      expect(location.path()).toBe('/about');
    });
  });

  it('navigate to "welcome" redirects you to "welcome"', () => {
    router.navigate(['welcome']).then( () => {
      expect(location.path()).toBe('/welcome');
    });
  });

  it('navigate to "welcome/admin" redirects you to "welcome/admin"', () => {
    router.navigate(['welcome/admin']).then( () => {
      expect(location.path()).toBe('/welcome/admin');
    });
  });

  it('navigate to "welcome/about" redirects you to "welcome/about"', () => {
    router.navigate(['welcome/about']).then( () => {
      expect(location.path()).toBe('/welcome/about');
    });
  });
});
