import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PDStateService} from './services/state/pdstateinterface';
import { PDStateHttpService} from './services/state/pdstatehttp.service';

@NgModule({
  providers: [
    {provide: PDStateService, useClass: PDStateHttpService}
  ],
  imports: [
    CommonModule,
  ],
  declarations: [],
  bootstrap: []
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
      throw new Error('CoreModule error: import ONLY in the AppModule');
    }
  }
}
