import { ConfigModel} from '../../models/configmodel';
import {HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

export interface IPDStateService {
  configData: ConfigModel;
  url: string;
  getConfiguration(): Observable<any>;
  handleError(error: HttpErrorResponse);
}

export abstract class PDStateService implements IPDStateService {
  abstract configData: ConfigModel;
  abstract url: string;
  abstract getConfiguration(): Observable<any>;
  abstract handleError(error: HttpErrorResponse);
}
