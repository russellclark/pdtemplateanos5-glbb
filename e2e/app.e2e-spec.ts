import { AppPage } from './app.po';

describe('pdtemplate-anos5-git-lab App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to PDTemplateANOS5-GitLab: Hosted on GitLab & GitLab Pages');
  });
});
