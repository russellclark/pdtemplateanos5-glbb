import { Injectable } from '@angular/core';
import { PDStateService } from './pdstateinterface';
import { ConfigModel } from '../../models/configmodel';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PDStateHttpService extends PDStateService {

  constructor(private _http: HttpClient) {
    super();
  }

  configData: ConfigModel;
  url = 'assets/config.json';

  getConfiguration(): Observable<any> {
    return this._http.get<ConfigModel>(this.url)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    console.log(`in error handler`);

    if (error.error instanceof ErrorEvent) {

       // A client-side or network error occurred. Handle it accordingly.
       console.error('Client-side error', error.error.message);
       console.error('  message:', error.error.message);

      // return an ErrorObservable with a user-facing error message
      return new ErrorObservable( 'Client: something bad happened; please try again later.');
    } else {

       // The backend returned an unsuccessful response code.
       // The response body may contain clues as to what went wrong,
       // perhaps we should make sure the body holds some useful info??
       console.error( `Backend error`);
       console.error( `  code: ${error.status} ${error.statusText}`);
       console.error( `  body:`);
       console.error( `  ${error.error}`);
       console.error( `  message: ${error.message}`);
       console.error( `  name: ${error.name}`);

      // return an ErrorObservable with a user-facing error message
      return new ErrorObservable( 'Server: something bad happened; please try again later.');
    }
  }
}
