// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDoc3g3cu6EvSS0M9sYtkID2PkRgB1HM8Q',
    authDomain: 'pdtemplateanos5-gitlab-dev.firebaseapp.com',
    databaseURL: 'https://pdtemplateanos5-gitlab-dev.firebaseio.com',
    projectId: 'pdtemplateanos5-gitlab-dev',
    storageBucket: 'pdtemplateanos5-gitlab-dev.appspot.com',
    messagingSenderId: '386564135342'
  }
};
