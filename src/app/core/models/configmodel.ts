export class ConfigModel {
  constructor() {
    this.error_code = '0';
  }
  apiServerURL: string;
  dbServerURL: string;
  logging_httpURL: string;
  logging_logglyID: string;

  error_code: string;
  error_server: string;
  error_client: string;

  addLogging(pHttpURP: string, ploggly: string) {
    this.logging_httpURL = pHttpURP;
    this.logging_logglyID = ploggly;
  }

  addError(pCode: string, pServer: string, pClient: string) {
    this.error_code = pCode;
    this.error_server = pServer;
    this.error_client = pClient;
  }
}


// this.configData = new ConfigModel();
// this.configData.addError('', '', '');
// this.configData.addLogging('../out-tsc/app', './');
//  this.configData.apiServerURL = '../tsconfig.json';
//  this.configData.dbServerURL = '../tsconfig.json';
