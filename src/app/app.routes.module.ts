import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './home/admin/admin.component';

// welcome is simply a demo of how to do child routes
export const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/about'},
  {path: 'about', component: HomeComponent},
  {path: 'admin', component: AdminComponent},
 {path: 'welcome',
    children: [
      {path: 'welcome', redirectTo: 'about', pathMatch: 'full'},
      {path: 'about', component: HomeComponent},
      {path: 'admin', component: AdminComponent}
   ]
  },
];

// enable tracing for dev only
// for an SPA useHash: true is best practice.
// Anything past the # does not get sent to the server
// but Angular also supports the PathLocationStrategy
// which needs some work to implement.
// See this: https://codecraft.tv/courses/angular/routing/routing-strategies/
@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: true, useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const routingComponents = [
  HomeComponent,
  AdminComponent
];
