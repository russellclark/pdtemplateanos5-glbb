import {TestBed, inject, async} from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PDStateHttpService } from './pdstatehttp.service';
import { ConfigModel } from '../../models/configmodel';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {HttpErrorResponse} from '@angular/common/http';

describe('PDStateHttpService', () => {
  let httpTestingController: HttpTestingController;
  let stateService: PDStateHttpService;
  let expectedConfig: ConfigModel;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ PDStateHttpService ]
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpTestingController = TestBed.get(HttpTestingController);
    stateService = TestBed.get(PDStateHttpService);
  }));

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', inject([PDStateHttpService],
     (service: PDStateHttpService) => {
        expect(service).toBeTruthy();
     })
  );

  // GET Tests
  describe('#getConfiguration', () => {

    beforeEach(async(() => {
      stateService = TestBed.get(PDStateHttpService);
      expectedConfig = new ConfigModel();
      expectedConfig.apiServerURL = 'russell';
    }));

    // 1) tell the mock what request we expect & what the url is
    it('should return expected configuration (called once)', () => {
      stateService.getConfiguration().subscribe(config => {
        console.log('config: ', config);
        expect(config).toEqual(expectedConfig, 'should return expected configuration');
        expect(config.apiServerURL).toEqual('russell');
      });

     // 2) stateService should have made one request to GET config from expected URL
     const req = httpTestingController.expectOne(stateService.url);
     expect(req.request.method).toEqual('GET');

      // Respond with the mock configuration
      req.flush(expectedConfig);

      // verify called
    });

    // i think this is crap
    it('should return error', () => {
      stateService.url = 'assets/fakeurl.json';
      console.log('fake url: ',  stateService.url);

      stateService.getConfiguration().subscribe(null, (response: HttpErrorResponse) => {
        console.log('response: ', response);
        expect(response.message).toBe('should return error');
        expect(response.status).toBe(404);
      });

      // 2) stateService should have made one request to GET config from expected URL
      const req = httpTestingController.expectOne('assets/fakeurl.json');
      expect(req.request.method).toEqual('GET');

      // Respond with the mock configuration
      req.flush(expectedConfig);

      // verify called
    });
  });
});

