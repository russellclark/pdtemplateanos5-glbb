# PDTemplateANOS5-GLBB

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Hosted projects: 
## Semi-Automated Deployment
### <a target="_blank" rel="noopener" href="https://github.com/RussellRClark/Angular.git">GitHub Repo</a>
### <a target="_blank" rel="noopener" href="https://pdtemplateanos5.firebaseapp.com/">FireBase</a> 
###  <a target="_blank" rel="noopener" href="https://russellrclark.github.io/Angular/">GitHub Pages</a>

## Continuous Integration & Deployment
Here I demonstrate a complete automated CI & CD triggered on push to the repo.
### <a target="_blank" rel="noopener" href="https://gitlab.com/RussellClark/PDTemplateANOS5_GitLab">GitLab Repo</a>
###  <a target="_blank" rel="noopener" href="https://gitlab.com/RussellClark/PDTemplateANOS5_GitLab">GitLab Pages</a>

   

