import { Injectable } from '@angular/core';
import { PDStateService } from './pdstateinterface';
import { ConfigModel } from '../../models/configmodel';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {catchError, retry} from 'rxjs/operators';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PDStateMockService extends PDStateService {

  constructor(private _http: HttpClient) {
    super();
  }

  configData: ConfigModel;

  url = 'assets/config.json';

   getConfiguration(): Observable<any> {
     return this._http.get<ConfigModel>(this.url)
       .pipe(
        catchError(this.handleError)
     );
   }

  handleError(error: HttpErrorResponse) {
    console.log(`in error handler`);

    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable( 'Mock: something bad happened; please try again later.');
  }
}
