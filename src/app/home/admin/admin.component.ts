import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public title = 'Hello Admin Page';
  public subtitle = 'Nothing to see here: just a demo Admin Page';

  constructor() {

  }

  ngOnInit() {
  }

}
