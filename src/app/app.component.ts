import { Component } from '@angular/core';
import { PDStateService} from './core/services/state/pdstateinterface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PDTemplateANOS5-GL-BB: Hosted on GitLab & BitBucket';

  constructor(private state: PDStateService) {
    // place outside of the constructor to allow testing & replay from the UI
    this.getConfiguration();
    console.log('configData 1 not async: ', this.state.configData);
  }

  getConfiguration() {
    this.state.getConfiguration()
      .subscribe(
        resp => {
          this.state.configData = resp;
          this.displayConfig();
        },
        error => this.handleError(error)
      );
  }

  displayConfig() {
    // async code configData is available here
    console.log('app got response body async configData: ', this.state.configData);
  }

   handleError(error) {
    // now we have the error we can do display it nicely to the user
    // we can log the error here or in the service (better)
    console.log('observable error: ', error);
  }
}
