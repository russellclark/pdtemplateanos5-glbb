import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { PDStateService } from './core/services/state/pdstateinterface';
import { PDStateMockService } from './core/services/state/pdstatemock.service';
import {HttpClient, HttpHandler} from '@angular/common/http';
import { RouterTestingModule} from '@angular/router/testing';

describe('AppComponent', () => {
  const angularFireDatabaseStub = { list: () => {} };
 // const stateService = TestBed.get(PDStateHttpService);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [ RouterTestingModule.withRoutes([{path: '#', component: AppComponent}])],
      providers: [ {provide: PDStateService, useClass: PDStateMockService},
      HttpClient, HttpHandler],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'PDTemplateANOS5-GL-BB: Hosted on GitLab & BitBucket'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('PDTemplateANOS5-GL-BB: Hosted on GitLab & BitBucket');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('PDTemplateANOS5-GL-BB: Hosted on GitLab & BitBucket');
  }));

  describe('#getConfiguration', () => {
    it('todo: test getConfiguration', async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('h1').textContent).toContain('PDTemplateANOS5-GL-BB: Hosted on GitLab & BitBucket');
    }));
  });
});




